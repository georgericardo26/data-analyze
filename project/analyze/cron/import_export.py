import json
import os

from concurrent.futures import ThreadPoolExecutor
from django.conf import settings
from datetime import datetime

from analyze.utils import to_isoformat
from . import handle_file


MAX_WORKERS = 10
path_in = settings.DATA_PATH_IN
path_out = settings.DATA_PATH_OUT


def import_file():
    """
    Functions to import files,
    calling export_file function to export files
    """
    
    execution = handle_file.ManagerDataFile()
    list_files = [
        file
        for file in os.listdir(path_in)
    ]

    if execution.check_if_file_exists(
            path=path_out,
            filename='log.txt'
    ):
        # Check if new files exists
        files_in_file_log = [
            json.loads(item)['original_file_name'] for item in
            execution.print_objects_from_log_file(file='log.txt')
        ]
        
        # check difference between lists
        list_files = list(set(list_files) - set(files_in_file_log))
        
    # Run Threads to handle and export json file
    
        list_dict = list(
            map(
                execution.check_file_and_split_lines,
                list_files
            )
        )

        export_file(list_dict, execution)


def export_file(
        list_dict: list,
        execution: handle_file.ManagerDataFile
):
    """
    Functions to export files,
    sending it to data/out directory to generate reports
    """
    
    # check logfile exists
    count_file = 0

    if execution.check_if_file_exists(path=path_out, filename='log.txt'):
        count_file = len(execution.print_objects_from_log_file(file='log.txt'))

    idx = count_file + 1
    for index, item in enumerate(list_dict):
        
        original_file_name = item.pop('original_file_name')
        
        # create json file
        execution.create_file_and_export(
            file_name=idx,
            item=item,
            format='json'
        )
        
        # insert database log
        obj_log = {
            'original_file_name': original_file_name,
            'file_name': idx,
            'create_date': to_isoformat(datetime.now())
        }
        execution.insert_log_to_database(obj_log)

        # insert log
        execution.create_log_file_and_export(
            file_name='log.txt',
            item=obj_log
        )

        idx += 1


def main():
    import_file()


if __name__ == '__main__':
    main()
