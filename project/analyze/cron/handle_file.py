import json
import os
from django.conf import settings

from ..models import ReportDataLog


class ManagerDataFile(object):
    
    """
    Class defined to build json object and handle
    each item type. the types are: '001(sellers), 002(clients), 003(sales)'
    """
    path_in = settings.DATA_PATH_IN
    path_out = settings.DATA_PATH_OUT
    
    def __init__(self):
        self.final_json = {}
        
    def check_file_and_split_lines(self, file: str) -> dict:
        self.final_json['original_file_name'] = file
        lines_dict = {
            '001': [],
            '002': [],
            '003': []
        }
        
        file_open = open(os.path.join(self.path_in, file), 'r')
        for line in file_open:
            [col1, col2, col3, col4] = line.split('ç')
            lines_dict[col1].append(
                (col2, col3, col4)
            )

        self._handle_sellers(lines_dict.pop('001'))
        self._handle_clients(lines_dict.pop('002'))
        self._handle_sales(lines_dict.pop('003'))
        
        return self.final_json

    def _handle_sellers(self, sellers: list):
        names = ['CPF', 'name', 'salary']
        self.final_json["sellers"] = {}
        self.final_json["sellers"]["quantity"] = len(sellers)
        self.final_json["sellers"]["list_sellers"] = self._build_clients_sellers_object(
            items=sellers,
            names=names
        )

    def _handle_clients(self, clients: list):
        names = ['CNPJ', 'name', 'business_area']
        self.final_json["clients"] = {}
        self.final_json["clients"]["quantity"] = len(clients)
        self.final_json["clients"]["list_clients"] = self._build_clients_sellers_object(
            items=clients,
            names=names
        )
        
    def _handle_sales(self, sales: list):
        names = ['sale_id', 'items', 'saleman']
        self.final_json["sales"] = {}
        self.final_json["sales"]["quantity"] = len(sales)
        self.final_json["sales"]["list_sales"] = []
        self.final_json["sales"]["list_sales"] = self._build_sales_object(
            items=sales,
            names=names
        )

    def _build_clients_sellers_object(
            self,
            items: list,
            names: list
    ) -> list:
        final_list = []
    
        for index, value in enumerate(items):
            final_list.append(
                {
                    names[index]: value for index, value
                    in enumerate(value)
                }
            )
    
        return final_list

    def _build_sales_object(
            self,
            items: list,
            names: list
    ) -> list:
        final_list = []
    
        for index, value in enumerate(items):
            obj_json = {}
            for index, value in enumerate(value):
                if index is not 1:
                    obj_json[names[index]] = value
                else:
                    obj_json[names[index]] = []
                    name_items = ['id', 'quantity', 'price']
                    item = value.strip('[]').split(',')
                    for index_list, value_list in enumerate(item):
                        obj_json[names[index]].append(
                            {
                                name_items[index_item]: value_item
                                for index_item, value_item in enumerate(value_list.split('-'))
                            }
                        )
                final_list.append(obj_json)
        return final_list
    
    def check_if_file_exists(self, path: str, filename: str) -> bool:
        return os.path.isfile(
            os.path.join(
                path,
                filename
            )
        )
    
    def print_objects_from_log_file(self, file: str) -> list:
    
        return [
                item for item in open(
                    os.path.join(
                        self.path_out,
                        file
                    ), 'r'
                )
               ]
        
    def list_quantity_file_into_dir(self, path: str) -> int:
        return len(
            [
                file
                for file in os.listdir(path)
            ]
        )
    
    def create_file_and_export(self, file_name: any, item: dict, format: str):
        json_file = open(
            os.path.join(
                self.path_out,
                '%s.%s' % (file_name, format)
            ),
            'w'
        )
        json_file.write(
            json.dumps(
                item
            )
        )
        json_file.close()
        
    def create_log_file_and_export(self, file_name: any, item: dict):
        log_file = open(
            os.path.join(
                self.path_out,
                file_name
            ),
            'a',
        )
        log_file.write(
            "{}\n".format(
                json.dumps(item)
            )
        )
        log_file.close()
    
    def insert_log_to_database(self, json_log: dict):
        """"
        Insert the json log on database
        """
        ReportDataLog.objects.create(**json_log)
