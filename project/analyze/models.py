from django.db import models

class ReportDataLog(models.Model):
    """
    Model to store log registers after have exported new file
    """
    original_file_name = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        help_text='Original file imported'
    )
    file_name = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        help_text='New filename created into out directory'
    )
    create_date = models.DateTimeField(
        null=True,
        blank=True,
        auto_now_add=True,
        auto_created=True
    )