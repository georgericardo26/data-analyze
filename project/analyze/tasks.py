from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from analyze.models import ReportDataLog
from .cron.import_export import import_file

logger = get_task_logger(__name__)


@periodic_task(run_every=15, name="import_export_file", ignore_result=True)
def import_export_file():
    
    """
    Task file for set a periodic task using celery
    """
    
    import_file()
    logger.info("Exported file successfully!")