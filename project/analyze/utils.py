import datetime


def to_isoformat(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()