import json
import os
from django.conf import settings

"""
File created for handle output to print reports
"""
path_in = settings.DATA_PATH_IN
path_out = settings.DATA_PATH_OUT


def check_file_and_return_data(file):
    file_open = open(os.path.join(path_out, '%s.json' % file), 'r')
    json_out = ''
    for line in file_open:
        json_out = line
    return json.loads(json_out)

    
