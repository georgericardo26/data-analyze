from django.contrib import admin
from .models import ReportDataLog


class ReportDataLogAdmin(admin.ModelAdmin):
    class Meta:
        model = ReportDataLog
    
    search_fields = ('original_file_name', 'file_name',)
    ordering = ('-create_date',)
    list_display = ('original_file_name', 'file_name', 'create_date',)
    list_filter = ('create_date',)


admin.site.register(ReportDataLog, ReportDataLogAdmin)