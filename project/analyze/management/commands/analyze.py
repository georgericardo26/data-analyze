from django.core.management.base import BaseCommand, CommandError
from analyze.cron.import_export import import_file


class Command(BaseCommand):
    help = 'Execute import, handle and export of files'
    
    def add_arguments(self, parser):
        parser.add_argument(
            '--run',
            action='store_true'
        )

    def handle(self, *args, **options):
        """
        Command for execute import and export files
        """

        if options['run']:
            import_file()
            self.stdout.write(self.style.SUCCESS('Files were handled and exported Successfully.'))
        else:
            self.stdout.write(self.style.ERROR('You need type run after analyze command.'))


