import json

from django.core.management.base import BaseCommand, CommandError

from analyze.cron.handle_file import ManagerDataFile
from analyze.report.handle_output import check_file_and_return_data


class Command(BaseCommand):
    help = 'Help us to list all reports files, clients, sellers and sales for command line directly'

    def add_arguments(self, parser):
        parser.add_argument(
            'subcommand',
            choices=[
                'list',
                'clients',
                'sellers',
                'best_sale',
                'worse_saller'
            ],
            help='Indicate all options to print report'
        )
        parser.add_argument(
            'label',
            nargs='?',
            help='Indicate which what report wants reading',
        )
        
    def handle(self, *args, **options):
        """
        Dispatches by given subcommand
        """
        
        if options['subcommand'] == 'list':
            
            execution = ManagerDataFile()
            
            # Check files from out directory
            files_in_file_log = [
                json.loads(item)['file_name'] for item in
                execution.print_objects_from_log_file(file='log.txt')
            ]
            self.stdout.write('#######################################################')
            self.stdout.write(' Please choose one of this report files')
            self.stdout.write('#######################################################')
            for index, item in enumerate(files_in_file_log):
                self.stdout.write('%d - %s.json' % (index+1, item))
            
            self.stdout.write(self.style.SUCCESS('Successfully list'))
            
        elif options['subcommand'] == 'clients':
    
            self.stdout.write('#######################################################')
            self.stdout.write('Report clients, loading...')
            self.stdout.write('#######################################################')
            
            try:
                json_file = check_file_and_return_data(options['label'])
                self.stdout.write('---------------------------------------------')
                self.stdout.write('QUANTITY: %s' % json_file['clients']['quantity'])
                self.stdout.write('---------------------------------------------')
                for index, item in enumerate(json_file['clients']['list_clients']):
                    self.stdout.write('CLIENT %d: ' % (index + 1))
                    for index_c, item_c in item.items():
                        self.stdout.write('  %s: %s' % (index_c, item_c))
            except Exception:
                self.stdout.write(self.style.ERROR('Not possible continue on this option.'))
            
            self.stdout.write(self.style.SUCCESS('Successfully clients'))
        
        elif options['subcommand'] == 'sellers':
            
            self.stdout.write('#######################################################')
            self.stdout.write('Report sellers, loading...')
            self.stdout.write('#######################################################')
    
            try:
                json_file = check_file_and_return_data(options['label'])
                self.stdout.write('---------------------------------------------')
                self.stdout.write('QUANTITY: %s' % json_file['sellers']['quantity'])
                self.stdout.write('---------------------------------------------')
               
                for index, item in enumerate(json_file['sellers']['list_sellers']):
                    self.stdout.write('SELLER %d: ' % (index + 1))
                    for index_c, item_c in item.items():
                        self.stdout.write('  %s: %s' % (index_c, item_c))
            except Exception:
                self.stdout.write(self.style.ERROR('Not possible continue on this option.'))
    
            self.stdout.write(self.style.SUCCESS('Successfully clients'))

        elif options['subcommand'] == 'best_sale':
            self.stdout.write('#######################################################')
            self.stdout.write('Best sale, loading...')
            self.stdout.write('#######################################################')
            
            try:
                json_file = check_file_and_return_data(options['label'])
    
                sales = {}
                for index, item in enumerate(json_file['sales']['list_sales']):
                    sales[item['sale_id']] = sum([
                        (int(item_sale['quantity']) * float(item_sale['price'])) for item_sale
                        in item['items']
                    ])
    
                self.stdout.write('---------------------------------------------')
                self.stdout.write(
                    (
                        'The best sale is the sale with id: %s \n '
                        'and the value is: %s'
                    )
                    % (max(sales, key=sales.get), sales[max(sales, key=sales.get)])
                )
                self.stdout.write('---------------------------------------------')
                self.stdout.write(self.style.SUCCESS('Successfully best sale'))
            except Exception:
                self.stdout.write(self.style.ERROR('Not possible continue on this option.'))
        
        elif options['subcommand'] == 'worse_saller':
            self.stdout.write('#######################################################')
            self.stdout.write('Worse saller, loading...')
            self.stdout.write('#######################################################')
    
            try:
                json_file = check_file_and_return_data(options['label'])
        
                sales = {}
                for index, item in enumerate(json_file['sales']['list_sales']):
                    sales[item['saleman']] = sum([
                        (int(item_sale['quantity']) * float(item_sale['price'])) for item_sale
                        in item['items']
                    ])
        
                self.stdout.write('---------------------------------------------')
                self.stdout.write(
                    (
                        'The worse saller is: %s \n '
                        'and the value of this sale is: %s'
                    )
                    % (min(sales, key=sales.get), sales[min(sales, key=sales.get)])
                )
                self.stdout.write('---------------------------------------------')
                self.stdout.write(self.style.SUCCESS('Successfully worse sale'))
            except Exception:
                self.stdout.write(self.style.ERROR('Not possible continue on this option.'))