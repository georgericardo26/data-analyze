from django.urls import path

from . import views

urlpatterns = [
    path('report/', views.ListLogDataView.as_view(), name='list-logfile'),
    path('report/<slug:filename>/', views.RetrieveLogDataView.as_view(), name='retrieve-report')
]