from django.http import HttpRequest
from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers
from rest_framework.reverse import reverse

from analyze.models import ReportDataLog

# request = HttpRequest()
# request.META['SERVER_NAME'] = HttpRequest.
# request.META['SERVER_PORT'] = '80'


class ListLogDataSerializer(serializers.ModelSerializer):
    
    """
    List all of report files printing your url
    """
    url = serializers.SerializerMethodField()

    class Meta:
        model = ReportDataLog
        field = '__all__'
        exclude = ()

    def get_url(self, obj):
        request = self.context['request']
        return request.build_absolute_uri(reverse('retrieve-report', kwargs={'filename': obj.file_name}))

