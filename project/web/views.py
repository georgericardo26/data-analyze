import json

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status

from analyze.report.handle_output import check_file_and_return_data
from .serializers import ListLogDataSerializer
from analyze.models import ReportDataLog


class ListLogDataView(generics.ListAPIView):
    serializer_class = ListLogDataSerializer
    queryset = ReportDataLog.objects.all()


@method_decorator(
    name='get',
    decorator=swagger_auto_schema(
        responses={},
    ),
)
class RetrieveLogDataView(generics.RetrieveAPIView):
    
    def get(self, request, *args, **kwargs):
        
        try:
            file = check_file_and_return_data(
                    kwargs['filename']
                )
            
            return HttpResponse(file)
        except Exception:
            return HttpResponse(
                status.HTTP_404_NOT_FOUND,
                'File not found')


class AboutTemplateView(TemplateView):
    template_name = "about.html"

    
